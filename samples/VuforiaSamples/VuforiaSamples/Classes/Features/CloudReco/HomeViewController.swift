//
//  HomeViewController.swift
//  VuforiaSamples
//
//  Created by Patomphong Wongkalasin on 10/12/2559 BE.
//  Copyright © 2559 Qualcomm. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let btImageSearch = UIButton()
        btImageSearch.frame = CGRect(x: 50, y: 50, width: 50, height: 50)
        btImageSearch.backgroundColor = UIColor.cyan
        btImageSearch.addTarget(self, action: #selector(pressButtonImageSearch), for: .touchUpInside)
        
        self.view.addSubview(btImageSearch)
        
        
    }
    
    @IBAction func pressButtonImageSearch() {
        print("pressButtonImageSearch")
        
        // Create a variable that you want to send based on the destination view controller
        // You can get a reference to the data by using indexPath shown below
       // let selectedProgram = programy[indexPath.row]
        
        // Create an instance of PlayerTableViewController and pass the variable
       // let destinationVC = TestViewController()
      //  destinationVC.programVar = selectedProgram
        
        // Let's assume that the segue name is called playerSegue
        // This will perform the segue and pre-load the variable for you to use
      //  destinationVC.performSegue(withIdentifier: "ImageSearchTemp", sender: self)
        
        self.performSegue(withIdentifier: "ImageSearchTemp", sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "ImageSearchTemp"){
            
            print("prepare segue")
            
            //prepare for segue to the details view controller
            
//            if let detailsVC = (sender? as AnyObject).destinationViewController as? TestViewController {
////                let indexPath = self.tableView.indexPathForSelectedRow
////                detailsVC.book = self.books[indexPath!.row]
//            }
            
            let destinationVC = TestViewController()
            destinationVC.action = "ImageSearch"
            
        }
    }
 

}
