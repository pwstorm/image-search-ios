//
//  SearchResultViewController.swift
//  VuforiaSamples
//
//  Created by Patomphong Wongkalasin on 10/12/2559 BE.
//  Copyright © 2559 Qualcomm. All rights reserved.
//

import UIKit

class SearchResultViewController: UIViewController {

    var productId : String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        print("Product ID = \(productId)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
