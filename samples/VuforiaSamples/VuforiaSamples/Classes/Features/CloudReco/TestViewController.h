//
//  TestViewController.h
//  VuforiaSamples
//
//  Created by Patomphong Wongkalasin on 10/12/2559 BE.
//  Copyright © 2559 Qualcomm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController

@property (nonatomic, strong) NSString* action;

@end
