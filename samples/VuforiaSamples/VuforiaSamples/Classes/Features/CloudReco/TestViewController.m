//
//  TestViewController.m
//  VuforiaSamples
//
//  Created by Patomphong Wongkalasin on 10/12/2559 BE.
//  Copyright © 2559 Qualcomm. All rights reserved.
//

#import "TestViewController.h"

@interface TestViewController ()

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    NSLog(@"TestViewController");
    NSLog(@"Action = %@", _action);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier: @"ImageSearch" sender: self];
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
